/* monochrome.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Monochrome layer format support

// Imports ===================================================================
use std::fs::File;
use std::io::Write;
use image::{DynamicImage, GenericImageView};
use crate::{LayerWriter, Options};

// Declarations ==============================================================
pub struct Rgba8LayerWriter {}

// Code ======================================================================
impl LayerWriter for Rgba8LayerWriter {
  fn layer_size_bytes(image: &DynamicImage) -> u32 {
    0
  }

  fn write_layer(output: &mut File, source: &DynamicImage, stdout: bool) {
    // todo!()
  }

  fn should_skip(options: &Options) -> bool {
    options.no_rgba8
  }

  fn friendly_name() -> &'static str {
    "RGBA8"
  }
}

// Tests =====================================================================
