/* monochrome.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Monochrome layer format support

// Imports ===================================================================
use std::fs::File;
use std::io::Write;
use image::{DynamicImage, GenericImageView};
use crate::{LayerWriter, Options};

// Declarations ==============================================================
pub struct MonochromeLayerWriter {}

// Code ======================================================================
impl LayerWriter for MonochromeLayerWriter {
  fn layer_size_bytes(image: &DynamicImage) -> u32 {
    let dimensions = image.dimensions();

    let rows = (((dimensions.1-1) / 8) + 1) * 8;
    let cols = ((dimensions.0-1) / 8) + 1;

    rows * cols
  }

  fn write_layer(output: &mut File, source: &DynamicImage, stdout: bool) {
    let dimensions = source.dimensions();

    let output_cols = ((dimensions.0-1) / 8) + 1;
    let output_rows = ((dimensions.1-1) / 8) + 1;

    let image = source.clone().into_luma8();

    for row in 0..output_rows {
      for col in 0..output_cols {
        let start_x = col * 8;
        let start_y = row * 8;
        let mut buffer = [ 0x00u8; 8 ];
        let mut offset = 0usize;

        for pix_y in start_y..(start_y+8) {
          let mut byte = 0x00u8;

          if pix_y < dimensions.1 {
            for pix_x in start_x..(start_x+8) {
              byte <<= 1;

              if pix_x < dimensions.0 {
                let pixel = image.get_pixel(pix_x,pix_y);

                if pixel.0[0] > 128 {
                  byte |= 0x01;
                }
              }
            }
          }
          buffer[offset] = byte;
          offset += 1;
        }

        output.write_all(&buffer).unwrap();

        if stdout {
          print!("  ");
          for byte in buffer {
            print!("0x{:x}u8, ", byte);
          }
          println!("// cell @ ({},{}) -> ({},{})", start_x, start_y, (start_x+7), (start_y+7));
        }
      }
    }
  }

  fn should_skip(options: &Options) -> bool {
    options.no_monochrome
  }

  fn friendly_name() -> &'static str {
    "Monochrome"
  }
}

// Tests =====================================================================
