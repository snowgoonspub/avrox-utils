/* main.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! A simple commandline tool that will let us convert images into our
//! simple TBFF (Trivial Bitmap File Format) format for use in embedded
//! systems.

mod monochrome;
mod greyscale;
mod rgba4;
mod rgba8;

// Imports ===================================================================
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use image::{DynamicImage, GenericImageView};
use structopt::StructOpt;
use crate::greyscale::GreyscaleLayerWriter;
use crate::monochrome::MonochromeLayerWriter;
use crate::rgba4::Rgba4LayerWriter;
use crate::rgba8::Rgba8LayerWriter;


// Declarations ==============================================================
#[derive(Debug, StructOpt,Clone)]
pub struct Options {

  /// Output the data to stdout as a Rust static array declaration that can be
  /// cut'n'pasted into your code
  #[structopt(long)]
  static_array: bool,

  /// The source file to convert
  source_path: PathBuf,

  /// The destination file to convert
  dest_path: PathBuf,

  /// Don't output the Monochrome (1bit) layer
  #[structopt(long)]
  no_monochrome: bool,
  /// Don't output the Greyscale (8bit) layer
  #[structopt(long)]
  no_greyscale: bool,
  /// Don't output the 4-bit RGBA layer
  #[structopt(long)]
  no_rgba4: bool,
  /// Don't output the 8-but RGBA layer
  #[structopt(long)]
  no_rgba8: bool
}

// Code ======================================================================
trait LayerWriter {

  /// Return the total - and exact - number of bytes that will be
  /// occupied by this layer
  fn layer_size_bytes(image: &DynamicImage) -> u32;

  /// Write the layer to the given output file
  ///
  /// If the `stdout` bool is set, will also output a Rust static text
  /// version to stdout.
  fn write_layer(ouput: &mut File, image: &DynamicImage, stdout: bool);

  /// True if we should skip writing this layer
  fn should_skip(options: &Options) -> bool;

  /// A friendly name for this format for debugging etc.
  fn friendly_name() -> &'static str;
}



const HEADER_OFFSET:u32 = 0x19;
const OUTPUT_FILE_VERSION:u8 = 2;

fn write_u32_bigendian(file: &mut File, val: u32, stdout: bool) -> Result<(),std::io::Error>{
  let buf = [
    ((val & 0xff000000) >> 24) as u8, // MSB
    ((val & 0x00ff0000) >> 16) as u8, //  .
    ((val & 0x0000ff00) >> 8) as u8,  //  .
    ((val & 0x000000ff) >> 0) as u8,  // LSB
  ];
  file.write_all(&buf)?;

  if stdout {
    for byte in buf {
      print!("0x{:x}u8, ", byte);
    }
  }

  Ok(())
}

fn write_index_entry<L: LayerWriter>(output: &mut File, source: &DynamicImage, layer_offset: &mut u32, options: &Options) {
  if !L::should_skip(options) {
    if options.static_array { print!("  ") }
    write_u32_bigendian(output, *layer_offset, options.static_array).unwrap();
    // @todo implement some of these
    *layer_offset += L::layer_size_bytes(&source);
    if options.static_array { println!("  // {} layer offset", L::friendly_name()) }
  } else {
    if options.static_array { print!("  ") }
    write_u32_bigendian(output, 0, options.static_array).unwrap();
    if options.static_array { println!("  // No {} layer", L::friendly_name()) }
  }
}

fn write_layer<L: LayerWriter>(output: &mut File, source: &DynamicImage, options: &Options) {
  if !L::should_skip(options) {
    if options.static_array { println!("\n  // {} layer data:", L::friendly_name()) }
    L::write_layer(output, source, options.static_array);
  } else {
    if options.static_array { println!("\n  // {} layer data skipped", L::friendly_name()) }
  }
}

fn main() {
  let options = Options::from_args();

  let source = image::open(options.source_path.clone()).unwrap();
  let mut output = File::create(options.dest_path.clone()).unwrap();

  let dimensions = source.dimensions();


  // Write the header out first
  let header_buf  = [
    b'T', b'B', b'F', b'G',
    OUTPUT_FILE_VERSION, // Version of the file spec we are outputting
    ((dimensions.0 & 0xff00u32) >> 8) as u8, // Width, 16b big-endian
    ((dimensions.0 & 0x00ffu32)) as u8,
    ((dimensions.1 & 0xff00u32) >> 8) as u8, // Height, 16b big-endian
    ((dimensions.1 & 0x00ffu32)) as u8,
  ];

  output.write_all(&header_buf).unwrap();
  if options.static_array {
    print!("static IMAGE = [\n  ");
    for byte in header_buf {
      print!("0x{:x}u8, ", byte);
    }
    println!("// Header, index follows");
  }

  // Now we write the layer index out
  {
    let mut layer_offset:u32 = HEADER_OFFSET;

    write_index_entry::<MonochromeLayerWriter>(&mut output, &source, &mut layer_offset, &options);
    write_index_entry::<GreyscaleLayerWriter>(&mut output, &source, &mut layer_offset, &options);
    write_index_entry::<Rgba4LayerWriter>(&mut output, &source, &mut layer_offset, &options);
    write_index_entry::<Rgba8LayerWriter>(&mut output, &source, &mut layer_offset, &options);
  }

  // And now write the layers
  write_layer::<MonochromeLayerWriter>(&mut output, &source, &options);
  write_layer::<GreyscaleLayerWriter>(&mut output, &source, &options);
  write_layer::<Rgba4LayerWriter>(&mut output, &source, &options);
  write_layer::<Rgba8LayerWriter>(&mut output, &source, &options);

  println!("];");
}
// Tests =====================================================================
